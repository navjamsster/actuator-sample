FROM azul/zulu-openjdk-alpine:11
LABEL maintainer = "navjamsster@gmail.com"
VOLUME /tmp
ADD /target/actuator-sample-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]